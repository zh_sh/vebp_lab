package mk.ukim.finki.wp.lab.web.controller;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Role;
import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.StudentService;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.WebContext;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Optional;

@Controller
@RequestMapping(value = {"/", "/courses"})
public class CourseController {

    private final CourseService courseService;
    private final TeacherService teacherService;
    private final StudentService studentService;

    public CourseController(CourseService courseService, TeacherService teacherService, StudentService studentService) {
        this.courseService = courseService;
        this.teacherService = teacherService;
        this.studentService = studentService;
    }

    @GetMapping("/listCourses")
    public String getCoursesPage(@RequestParam(required = false) String error, Model model){

        if(!teacherService.getTeacherById(1L).isPresent()){
            teacherService.saveTeacher( 1L, "dummyteacher","b", null);
        }

        model.addAttribute("courses", courseService.listAll());

        return "asdf.html";
    }

    @PostMapping("/listCourses")
    public String courseSelected(@RequestParam(required = false) String error, Model model, HttpServletRequest req){
        String attributeValue = req.getParameter("courseId");
        req.getSession().setAttribute("courseId", attributeValue);
        model.addAttribute("courseId", attributeValue); //just in case
//        resp.sendRedirect("/addStudent");
        return "redirect:/addStudent";
    }

    @GetMapping("/add-form")
    public String addCourse(Model model){

        model.addAttribute("teachers",teacherService.findAll());

        return "add-course";
    }

    @PostMapping("/add")
    public String saveCourse(@RequestParam String name, @RequestParam String description, @RequestParam Long teacherId, @RequestParam Long courseId, @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date){

        if(courseId == -1){
            courseId = (long) (Math.random() * 1000);
        }
        courseService.addCourse(courseId, name, description, teacherId, date);

//        return "redirect:/courses";
        return "redirect:/listCourses";
    }

//    @RequestMapping("/delete/{id}")
    @GetMapping("/delete/{id}")
    public String deleteCourse(@PathVariable Long id){

        courseService.removeCourseById(id);

        return "redirect:/courses";
//        return "redirect:/listCourses";

    }

    @GetMapping("/edit-form/{id}")
    public String editCourse(@PathVariable Long id, Model model){

        Optional<Course> tempCourse = this.courseService.getCourseById(id);

//        if(this.courseService.getCourseById(id).isPresent()){
        if(tempCourse.isPresent()){
//            model.addAttribute("course",courseService.getCourseById(id).get());
            model.addAttribute("course",tempCourse.get());
            model.addAttribute("teachers",teacherService.findAll());

            Optional<Teacher> tempTeacher = courseService.getFirstTeacherByCourse(id);
//            Optional<Teacher> tempTeacher = Optional.of(tempCourse.get().getTeachers().get(0));
            if(tempTeacher.isPresent()){
                model.addAttribute("teacher", tempTeacher.get());
            }else{
                model.addAttribute("teacher",null);
            }
            System.out.println(model.getAttribute("teacher"));

            return "add-course";
        }else{
            return "redirect:/courses?error=CourseNotFound";
        }

    }

    @GetMapping("/studentList/{courseId}")
    public String getStudents(@RequestParam(required = false) String error, @PathVariable Long courseId, Model model) {
        //listStudentsByCourse

        model.addAttribute("courseId", courseId);

        model.addAttribute("course",courseService.getCourseById(courseId).get());
        return "studentsInCourseV2.html";
    }


    @GetMapping("/addStudent")
    public String courseMainPage(@RequestParam(required = false) String error, Model model, HttpServletRequest req) {

        model.addAttribute("students",studentService.listAll());

        Long courseId = Long.parseLong(req.getSession().getAttribute("courseId").toString(),10);
        model.addAttribute("courseId", courseId);

        return "listStudents.html";
    }

    @GetMapping("/createStudent")
    public String createStudentPage(@RequestParam(required = false) String error, Model model, HttpServletRequest req) {

        return "createStudent.html";
    }

    @PostMapping("/createStudent")
    public String createStudentPostFunc(@RequestParam(required = false) String error, @RequestParam String username, @RequestParam String password, @RequestParam String name, @RequestParam String surname, Model model, HttpServletRequest req) {

        studentService.save(username, password, name, surname);
//        studentService.save(username, password, name, surname, Role.ROLE_USER);

//        Long courseId = Long.parseLong(req.getSession().getAttribute("courseId").toString(),10);
//        model.addAttribute("courseId", courseId);

        return "redirect:/addStudent";
    }


    @GetMapping("/StudentEnrollmentSummary")
    public String studentEnrollmentSummaryPage(@RequestParam(required = false) String error, Model model, HttpServletRequest req) {

        model.addAttribute("studentsInCurrentCourse",
                courseService.listStudentsByCourse(Long.parseLong(req.getSession().getAttribute("courseId").toString(),10)));

        return "studentsInCourse.html";
    }


    @PostMapping("/StudentEnrollmentSummary")
    public String studentEnrollmentPost(@RequestParam(required = false) String error, @RequestParam String selectedStudent, Model model, HttpServletRequest req) {

        Course currentCourse = courseService.addStudentInCourse(selectedStudent,Long.parseLong(req.getSession().getAttribute("courseId").toString(),10));

        req.getSession().setAttribute("currentCourseName" ,currentCourse.getName());
        req.getSession().setAttribute("currentCourseDescription", currentCourse.getDescription());

        return "redirect:/StudentEnrollmentSummary";
    }


}
