package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryTeacherRepository;
import mk.ukim.finki.wp.lab.repository.jpa.TeacherRepository;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImpl implements TeacherService {

//    private final InMemoryTeacherRepository teacherRepository;
    private final TeacherRepository teacherRepository;

    public TeacherServiceImpl(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Override
    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher saveTeacher(Long id, String name, String surname, LocalDate dateOfEmployment) {
        Teacher teacher = new Teacher(id, name, surname, dateOfEmployment);
//        Teacher teacher = new Teacher(name, surname, dateOfEmployment);
        return teacherRepository.save(teacher);
    }

    @Override
    public void deleteTeacherById(Long id) {
        teacherRepository.deleteById(id);
    }

    @Override
    public Optional<Teacher> getTeacherById(Long id) {
//        return Optional.of(teacherRepository.findById(id));
        return teacherRepository.findById(id);
    }
}
