package mk.ukim.finki.wp.lab.repository.impl;

import mk.ukim.finki.wp.lab.bootstrap.DataHolder;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class InMemoryTeacherRepository {

    public List<Teacher> findAll(){
        return DataHolder.teachers;
    }

    public Teacher findById(Long id){
        return DataHolder.teachers.stream().filter(a->a.getId() == id).findFirst().get();
    };

    public Teacher save(Teacher teacher){
        Optional<Teacher> tempTeacher = DataHolder.teachers.stream().filter(a->a.getId() == teacher.getId()).findFirst();

        if(tempTeacher.isPresent()){
            teacher.setId(tempTeacher.get().getId());
            DataHolder.teachers.remove(tempTeacher.get());
        }

        DataHolder.teachers.add(teacher);
        return teacher;
    };

    public void deleteById(Long id) {
        DataHolder.teachers.removeIf(a->a.getId() == id);
    }

}
