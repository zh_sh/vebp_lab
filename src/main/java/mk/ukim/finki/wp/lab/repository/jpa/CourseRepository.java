package mk.ukim.finki.wp.lab.repository.jpa;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
//    @Override
//    Course getById(Long aLong);

    @Override
    List<Course> findAll();

    @Override
    Optional<Course> findById(Long aLong);

    @Override
    void deleteById(Long aLong);

    Optional<Course> findCourseByCourseId(Long id);

//    List<Student> findAllStudentsByCourseId(Long id);

//    Optional<Teacher> findFirstByCourseId(Long id);
//    Optional<Teacher> findFirstTeacherByCourseId(Long id);
    Course findFirstByCourseId(Long id);

}
