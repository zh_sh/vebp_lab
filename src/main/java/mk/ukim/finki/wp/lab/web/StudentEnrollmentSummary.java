//package mk.ukim.finki.wp.lab.web;
//
//import mk.ukim.finki.wp.lab.model.Course;
//import mk.ukim.finki.wp.lab.model.Student;
//import mk.ukim.finki.wp.lab.service.CourseService;
//import mk.ukim.finki.wp.lab.service.StudentService;
//import org.thymeleaf.context.WebContext;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Optional;
//
//@WebServlet(name="",urlPatterns = "/StudentEnrollmentSummary")
//public class StudentEnrollmentSummary extends HttpServlet {
//    private final SpringTemplateEngine springTemplateEngine;
//    private final CourseService courseService;
//    private final StudentService studentService;
//
//    public StudentEnrollmentSummary(SpringTemplateEngine springTemplateEngine, CourseService courseService, StudentService studentService) {
//        this.springTemplateEngine = springTemplateEngine;
//        this.courseService = courseService;
//        this.studentService = studentService;
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        WebContext context = new WebContext(req,resp, req.getServletContext());
//
//        Long temp = Long.parseLong(req.getSession().getAttribute("courseId").toString(),10);
//
//        context.setVariable("studentsInCurrentCourse",
//                courseService.listStudentsByCourse(Long.parseLong(req.getSession().getAttribute("courseId").toString(),10))
//        );
//
////        context.setVariable("currentCourseName",req.getSession().getAttribute("currentCourseName"));
////        context.setVariable("currentCourseDescription",req.getSession().getAttribute("currentCourseDescription"));
//
//        springTemplateEngine.process("studentsInCourse",context,resp.getWriter());
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String studentUserName = req.getParameter("selectedStudent");
//        //if not null...
//        //Student student = studentService.searchUsername(studentUserName);
//
//        Course currentCourse = courseService.addStudentInCourse(studentUserName,Long.parseLong(req.getSession().getAttribute("courseId").toString(),10));
//
//        if(courseService.listStudentsByCourse(Long.parseLong(req.getSession().getAttribute("courseId").toString(),10)).contains(studentService.searchUsername(studentUserName))){
//            //selected student already exists
//        }
//
//        req.getSession().setAttribute("currentCourseName",currentCourse.getName());
//        req.getSession().setAttribute("currentCourseDescription",currentCourse.getDescription());
//
//        //selectedStudent
//        //searchUsername
//        resp.sendRedirect("/StudentEnrollmentSummary");
//    }
//}
