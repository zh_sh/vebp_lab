package mk.ukim.finki.wp.lab.bootstrap;

import lombok.Getter;
import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Getter
public class DataHolder {

    public static List<Student> students = new ArrayList<>();
    public static List<Course> courses = new ArrayList<>();
    public static List<Teacher> teachers = new ArrayList<>();

//    @PostConstruct
//    public void init(){
//        students.add(new Student("a","b","c","d"));
//        students.add(new Student("e","f","g","h"));
//        students.add(new Student("i","j","k","l"));
//        students.add(new Student("m","n","o","p"));
//        students.add(new Student("q","r","s","t"));
//
//        //courses.add(new Course(1L,"Веб програмирање","b"/*,students*/));
//        //courses.add(new Course(2L,"Оперативни системи","d"/*,students*/));
//        //courses.add(new Course(3L,"Електронска и мобилна трговија","f"/*,students*/));
//        //courses.add(new Course(4L,"Компјутерски мрежи","h"/*,students*/));
//        //courses.add(new Course(5L,"i","j"/*,students*/));
//
//        /*
//        //ova e ok
//        courses.add(new Course(1L,"a","b",students));
//        courses.add(new Course(2L,"c","d",students));
//        courses.add(new Course(3L,"e","f",students));
//        courses.add(new Course(4L,"g","h",students));
//        courses.add(new Course(5L,"i","j",students));
//        */
//
//        //i ova e ok
//        /*
//        courses.add(new Course(1L,"Veb Programiranje","b",students));
//        courses.add(new Course(2L,"Operativni Sistemi","d",students));
//        courses.add(new Course(3L,"Elektronska i mobilna trgovija","f",students));
//        courses.add(new Course(4L,"Kompjuterski mrezhi","h",students));
//        courses.add(new Course(5L,"i","j",students));
//        */
//
//        teachers.add(new Teacher("a", "b"));
//        teachers.add(new Teacher("c", "d"));
//        teachers.add(new Teacher("e", "f"));
//        teachers.add(new Teacher("g", "h"));
////        courses.add(new Course(1L,"Veb Programiranje","b",new ArrayList<>()));
////        courses.add(new Course(2L,"Operativni Sistemi","d",new ArrayList<>()));
////        courses.add(new Course(3L,"Elektronska i mobilna trgovija","f",new ArrayList<>()));
////        courses.add(new Course(4L,"Kompjuterski mrezhi","h",new ArrayList<>()));
////        courses.add(new Course(5L,"i","j",new ArrayList<>()));
//
//        courses.add(new Course(1L,"Veb Programiranje","b",teachers.get(0)));
//        courses.add(new Course(2L,"Operativni Sistemi","d",teachers.get(0)));
//        courses.add(new Course(3L,"Elektronska i mobilna trgovija","f",teachers.get(0)));
//        courses.add(new Course(4L,"Kompjuterski mrezhi","h",teachers.get(0)));
//        courses.add(new Course(5L,"i","j",teachers.get(0)));
//
//    }
}
