package mk.ukim.finki.wp.lab.web.controller;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.GradeService;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping(value = { "/grades"})
public class GradeController {
    private final GradeService gradeService;
    private final CourseService courseService;
    private final StudentService studentService;

    public GradeController(GradeService gradeService, CourseService courseService, StudentService studentService) {
        this.gradeService = gradeService;
        this.courseService = courseService;
        this.studentService = studentService;
    }

//    @RequestMapping
    @GetMapping("/{courseId}")
    public String getGradesByCourseId(@RequestParam(required = false) String error, @PathVariable Long courseId, Model model){

//        Course currentCourse = courseService.getCourseById(courseId).get();

        model.addAttribute("gradesInCurrentCourse",gradeService.listGradesByCourseId(courseId));

        return "gradesInCourse.html";
    }

//    @GetMapping("/{courseId}")
//    public String getGradesByCourseId(@RequestParam(required = false) String error, @PathVariable Long courseId, Model model){
//
////        Course currentCourse = courseService.getCourseById(courseId).get();
//
//        model.addAttribute("gradesInCurrentCourse",gradeService.listGradesByCourseId(courseId));
//
//        return "gradesInCourse.html";
//    }

    //..

    @GetMapping("/add-grade/{studentUsername}")
    public String addGrade(@RequestParam(required = false) String error, @PathVariable String studentUsername, Model model, HttpServletRequest req){

//        Course currentCourse = courseService.getCourseById(courseId).get();

        model.addAttribute("student",studentService.searchUsername(studentUsername));

//        List<Grade> gradesByStudentUsername = gradeService.listGradesByStudentUsername(studentUsername);
//        if(gradesByStudentUsername.size() != 0){
//            model.addAttribute("grade", gradesByStudentUsername.get(0));
//        }else{
//            model.addAttribute("grade", null);
//        }

//        Grade gradeOfCurrentStudentInCurrentCourse = gradeService.getGradeByCourseIdAndStudentUsername((Long) req.getSession().getAttribute("courseId"), studentUsername);
        Grade gradeOfCurrentStudentInCurrentCourse = gradeService.getGradeByCourseIdAndStudentUsername(Long.parseLong(req.getSession().getAttribute("courseId").toString(), 10), studentUsername);
        model.addAttribute("grade", gradeOfCurrentStudentInCurrentCourse);
//        model.addAttribute("studentUsername",studentUsername);

        return "add-grade.html";
    }

    //...

    @PostMapping("/add")
//    public String add(@RequestParam(required = false) String error, @RequestParam String studentUsername, @RequestParam Character gradeChar, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime gradingDate, Model model){
    public String add(@RequestParam(required = false) String error, @RequestParam String studentUsername, @RequestParam Character gradeChar, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime gradingDate, Model model, HttpServletRequest req, @RequestParam Long gradeId){

        if(gradeId == -1){
            gradeId = (long) (Math.random() * 1000);
        }

//        gradeService.addGrade((long) (Math.random() * 1000), gradeChar, studentUsername, (long) model.getAttribute("courseId"), gradingDate);
//        gradeService.addGrade((long) (Math.random() * 1000), gradeChar, studentUsername, Long.parseLong(req.getSession().getAttribute("courseId").toString(), 10), gradingDate);
        gradeService.addGrade(gradeId, gradeChar, studentUsername, Long.parseLong(req.getSession().getAttribute("courseId").toString(), 10), gradingDate);

//        return "redirect:/listCourses";
//        return "redirect:/add-grade/"+studentUsername;
        return "redirect:/addStudent";
    }

}
