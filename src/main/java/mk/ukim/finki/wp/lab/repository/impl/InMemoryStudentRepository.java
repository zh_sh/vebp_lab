package mk.ukim.finki.wp.lab.repository.impl;

import mk.ukim.finki.wp.lab.bootstrap.DataHolder;
import mk.ukim.finki.wp.lab.model.Student;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InMemoryStudentRepository {
    public List<Student> findAllStudents(){
        return DataHolder.students;
    }
    public List<Student> findAllByNameOrSurname(String text){
        return DataHolder.students.stream().filter(a->a.getName().contains(text)||a.getSurname().contains(text)).collect(Collectors.toList());
    }
    public Student findByUsername(String username){
        return DataHolder.students.stream().filter(a->a.getUsername().equals(username)).findFirst().get();
    }

    public Student save(Student student){

        if(student.getUsername().isEmpty() || student.getPassword().isEmpty() || student.getName().isEmpty() || student.getSurname().isEmpty()){
            return null;
        }

//        DataHolder.students.removeIf(a->a.equals(student));
        DataHolder.students.removeIf(a->a.getUsername().equals(student.getUsername()));
        DataHolder.students.add(student);
        return student;
    }
}
