package mk.ukim.finki.wp.lab.service;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CourseService {
    List<Course> listAll();
    List<Student> listStudentsByCourse(Long courseId);
    Course addStudentInCourse(String username, Long courseId);
//    Course addCourse(String name, String description, Long professorId);
//    Course addCourse(Long CourseId, String name, String description, Long professorId);
    Course addCourse(Long CourseId, String name, String description, Long professorId, LocalDate date);
//    Course addCourse(String name, String description, Long professorId, LocalDate date);
    void removeCourseById(Long id);
    Optional<Course> getCourseById(Long id);
    Optional<Teacher> getFirstTeacherByCourse(Long id);
}
