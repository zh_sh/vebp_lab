package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryCourseRepository;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryStudentRepository;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryTeacherRepository;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepository;
import mk.ukim.finki.wp.lab.repository.jpa.GradeRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.repository.jpa.TeacherRepository;
import mk.ukim.finki.wp.lab.service.CourseService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

//    private final InMemoryCourseRepository courseRepository;
//    private final InMemoryStudentRepository studentRepository;
//    private final InMemoryTeacherRepository teacherRepository;

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final TeacherRepository teacherRepository;
    private final GradeRepository gradeRepository;

    public CourseServiceImpl(CourseRepository courseRepository, StudentRepository studentRepository, TeacherRepository teacherRepository, GradeRepository gradeRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.teacherRepository = teacherRepository;
        this.gradeRepository = gradeRepository;
    }

    @Override
    public List<Course> listAll() {
//        return courseRepository.findAllCourses();
        return courseRepository.findAll();
    }

    @Override
    public List<Student> listStudentsByCourse(Long courseId) {
//        return courseRepository.findAllStudentsByCourse(courseId);
        return courseRepository.findCourseByCourseId(courseId).get().getStudents();
    }

    @Override
    @Transactional
    public Course addStudentInCourse(String username, Long courseId) {

        Course tempCourse = courseRepository.findCourseByCourseId(courseId).get();

//        if(courseRepository.findAllStudentsByCourse(courseId).contains(studentRepository.findByUsername(username))){
//        if(courseRepository.findAllStudentsByCourseId(courseId).contains(studentRepository.findByUsername(username))){
        if(tempCourse.getStudents().contains(studentRepository.findByUsername(username))){
            return courseRepository.findById(courseId).get();
        }

//        Student student = studentRepository.findByUsername(username);
//        return courseRepository.addStudentToCourse(student, courseRepository.findById(courseId).get());
        Student student = studentRepository.findByUsername(username);
        tempCourse.getStudents().add(student);
        return tempCourse;
    }

    /*
    @Override
    public Course addCourse(String name, String description, Long professorId) {

        Course course = new Course(name,description);

//        course.getTeachers().add(teacherRepository.findById(professorId));
        course.getTeachers().add(teacherRepository.findById(professorId).get());
        courseRepository.save(course);

        return course;
    }

    @Override
    public Course addCourse(Long courseId, String name, String description, Long professorId) {

//        Course course = new Course(courseId, name, description);
//        course.getTeachers().add(teacherRepository.findById(professorId));

//        Course course = new Course(courseId, name, description, teacherRepository.findById(professorId));
        Course course = new Course(courseId, name, description, teacherRepository.findById(professorId).get());

        courseRepository.save(course);

        return course;
    }
    */

    @Override
    public Course addCourse(Long courseId, String name, String description, Long professorId, LocalDate date) {

//        Course course = new Course(courseId, name, description, teacherRepository.findById(professorId), date);
        Course course = new Course(courseId, name, description, teacherRepository.findById(professorId).get(), date);

        courseRepository.save(course);

        return course;
    }

//    @Override
//    public Course addCourse(String name, String description, Long professorId, LocalDate dateEnrolled) {
//
//        Course course = new Course(name, description, teacherRepository.findById(professorId).get(), dateEnrolled);
//
//        courseRepository.save(course);
//
//        return course;
//    }

    @Override
    public void removeCourseById(Long id) {
//        courseRepository.deleteCourseById(id);
        gradeRepository.deleteByCourse(courseRepository.findById(id).orElse(null));
        courseRepository.deleteById(id);
    }

    @Override
    public Optional<Course> getCourseById(Long id) {
        return courseRepository.findById(id);
    }

    @Override
    public Optional<Teacher> getFirstTeacherByCourse(Long id) {
//        return courseRepository.findFirstTeacherByCourse(id);
//        return courseRepository.findFirstByCourseId(id);
//        return courseRepository.findFirstTeacherByCourseId(id);
//        Course tempCourse = courseRepository.findFirstByCourseId(id);
        Course tempCourse = courseRepository.findCourseByCourseId(id).get();
        return tempCourse.getTeachers().stream().findFirst();
    }

}
