package mk.ukim.finki.wp.lab.repository.impl;

import mk.ukim.finki.wp.lab.bootstrap.DataHolder;
import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class InMemoryCourseRepository {
    public List<Course> findAllCourses(){
        return DataHolder.courses;
    }
    public Optional<Course> findById(Long courseId){
        return DataHolder.courses.stream().filter(a-> Objects.equals(a.getCourseId(), courseId)).findFirst();
    }
    public List<Student> findAllStudentsByCourse(Long courseId){
        return DataHolder.courses.stream().filter(a->a.getCourseId().equals(courseId)).findFirst().get().getStudents();
    }
    public Course addStudentToCourse(Student student, Course course){
        if(student == null || student.getName().isEmpty() || student.getUsername().isEmpty() || student.getPassword().isEmpty() || course.getCourseId() == null){
            return null;
        }
        DataHolder.courses.stream().filter(a->a.equals(course)).findFirst().get().getStudents().removeIf(a->a.getUsername().equals(student.getUsername()));
        DataHolder.courses.stream().filter(a->a.equals(course)).findFirst().get().getStudents().add(student);
        return course;
    }

    public Course save(Course course) {

//        DataHolder.courses.removeIf(a->a.getCourseId().equals(course.getCourseId()));

//        Optional<Course> tempCourse = Optional.of(DataHolder.courses.stream().filter(a->a.getName().equals(course.getName())).findFirst().get());

//        Optional<Course> tempCourse = Optional.of(DataHolder.courses.stream().filter(a->a.getCourseId().equals(course.getCourseId())).findFirst().get());

        Optional<Course> tempCourse = DataHolder.courses.stream().filter(a->a.getCourseId().equals(course.getCourseId())).findFirst();

        if(tempCourse.isPresent()){
            course.setCourseId(tempCourse.get().getCourseId());
            DataHolder.courses.remove(tempCourse.get());
        }

//        if(tempCourse.isPresent()){
//            Long tempId = tempCourse.get().getCourseId();
//            DataHolder.courses.remove(tempCourse.get());
//            course.setCourseId(tempId);
//        }
//        DataHolder.courses.removeIf(a->a.getName().equals(course.getName()));

        DataHolder.courses.add(course);

        return course;
    }

    public void deleteCourseById(Long id) {
        DataHolder.courses.removeIf(a->a.getCourseId().equals(id));
    }

    public Optional<Teacher> findFirstTeacherByCourse(Long courseId){
//        return DataHolder.courses.stream().filter(a->a.getCourseId().equals(courseId)).findFirst().get().getTeachers().get(0);
        return DataHolder.courses.stream().filter(a->a.getCourseId().equals(courseId)).findFirst().get().getTeachers().stream().findFirst();
    }

}
