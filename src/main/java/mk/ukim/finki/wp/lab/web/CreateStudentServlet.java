//package mk.ukim.finki.wp.lab.web;
//
//import mk.ukim.finki.wp.lab.model.Student;
//import mk.ukim.finki.wp.lab.service.impl.Sessions;
//import mk.ukim.finki.wp.lab.service.impl.StudentServiceImpl;
//import org.thymeleaf.context.WebContext;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSessionEvent;
//import java.io.IOException;
//
//@WebServlet(name="",urlPatterns = "/createStudent")
//public class CreateStudentServlet extends HttpServlet {
//    private final SpringTemplateEngine springTemplateEngine;
//    private final StudentServiceImpl studentService;
//
//    public CreateStudentServlet(SpringTemplateEngine springTemplateEngine, StudentServiceImpl studentService) {
//        this.springTemplateEngine = springTemplateEngine;
//        this.studentService = studentService;
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        WebContext context = new WebContext(req,resp, req.getServletContext());
//
//        Long temp = Long.parseLong(req.getSession().getAttribute("courseId").toString(),10);
//
//        springTemplateEngine.process("createStudent",context,resp.getWriter());
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//        String username = req.getParameter("username");
//        String password = req.getParameter("password");
//        String name = req.getParameter("name");
//        String surname = req.getParameter("surname");
//
//        if(username == null || password == null || name == null || surname == null || username.isEmpty() || password.isEmpty() || name.isEmpty() || surname.isEmpty()){
////            req.setAttribute("invalidNewStudentInput",true);
//            resp.sendRedirect("/createStudent");
//        }else{
////            req.setAttribute("invalidNewStudentInput",false);
//            studentService.save(username,password,name,surname);
//
//            Long temp = Long.parseLong(req.getSession().getAttribute("courseId").toString(),10);
//
////            if(req.getSession().getAttribute("activeUsers") != null){
////                int currentActiveUsersTemp = (int) req.getSession().getAttribute("activeUsers");
////                req.getSession().setAttribute("activeUsers",++currentActiveUsersTemp);
////            }else{
////                req.getSession().setAttribute("activeUsers",1);
////            }
//
//            resp.sendRedirect("/addStudent");
//    //        resp.sendRedirect("/StudentEnrollmentSummary");
//
//        }
//    }
//}
