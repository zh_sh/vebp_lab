package mk.ukim.finki.wp.lab.service;

import mk.ukim.finki.wp.lab.model.Teacher;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TeacherService {
    List<Teacher> findAll();
//    Teacher saveTeacher(Long id, String name, String surname);
//    Teacher saveTeacher(String name, String surname, LocalDate dateOfEmployment);
    Teacher saveTeacher(Long id, String name, String surname, LocalDate dateOfEmployment);
    void deleteTeacherById(Long id);
    Optional<Teacher> getTeacherById(Long id);
}
