package mk.ukim.finki.wp.lab.web;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.service.CourseService;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name="Number-Of-Students-In-Courses",urlPatterns = "/studentsInCourses")
public class NumberOfStudentsInCourses extends HttpServlet {
    private final SpringTemplateEngine springTemplateEngine;
    private final CourseService courseService;

    public NumberOfStudentsInCourses(SpringTemplateEngine springTemplateEngine, CourseService courseService) {
        this.springTemplateEngine = springTemplateEngine;
        this.courseService = courseService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Course> allCourses = courseService.listAll();

//        req.setAttribute("AllCoursesList", allCourses);

//        req.setAttribute("AllStudentsInCoursesList",courseService.listStudentsByCourse());
//        req.setAttribute("AllStudentsInCoursesList",allCourses.stream().forEach(a->a.getStudents()));

        WebContext context = new WebContext(req, resp, req.getServletContext());
        context.setVariable("AllCoursesList",allCourses);

        springTemplateEngine.process("prvoBaranje.html",context,resp.getWriter());
    }
}
