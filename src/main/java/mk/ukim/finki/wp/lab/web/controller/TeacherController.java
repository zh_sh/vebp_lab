package mk.ukim.finki.wp.lab.web.controller;

import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/teachers")
public class TeacherController {
    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping()
    public String getTeachersPage(@RequestParam(required = false) String error, Model model){

        model.addAttribute("teachers",teacherService.findAll());

        return "teachers";
    }

    @GetMapping("/add-form")
    public String addTeacher(){
        return "add-teacher";
    }

    @PostMapping("/add")
    public String saveTeacher(@RequestParam String name, @RequestParam String surname, @RequestParam Long teacherId, @RequestParam("enrollmentDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfEmployment){
        if(teacherId == -1){
            teacherId = (long) (Math.random() * 1000);
        }
//        teacherService.saveTeacher(teacherId, name, surname);
        teacherService.saveTeacher(teacherId, name, surname, dateOfEmployment);
        return "redirect:/teachers";
    }

    @GetMapping("/edit-form/{id}")
    public String editTeacher(@PathVariable Long id, Model model){
        if(this.teacherService.getTeacherById(id).isPresent()){
            model.addAttribute("teacher",teacherService.getTeacherById(id).get());
            return "add-teacher";
        }else{
            return "redirect:/teachers?error=TeacherNotFound";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteTeacher(@PathVariable Long id){
        teacherService.deleteTeacherById(id);
        return "redirect:/teachers";
    }

}