//package mk.ukim.finki.wp.lab.web;
//
//import mk.ukim.finki.wp.lab.model.Course;
//import mk.ukim.finki.wp.lab.service.CourseService;
//import org.thymeleaf.context.WebContext;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@WebServlet(name = "courses-list-servlet", urlPatterns = "/listCourses")
//public class CoursesListServlet extends HttpServlet {
//    private final SpringTemplateEngine springTemplateEngine;
//    private final CourseService courseService;
//
//    public CoursesListServlet(SpringTemplateEngine engine, CourseService courseService) {
//        this.springTemplateEngine = engine;
//        this.courseService = courseService;
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//        WebContext context = new WebContext(req, resp, req.getServletContext());
//        context.setVariable("courses",this.courseService.listAll());
//
////        this.springTemplateEngine.process("listCourses.html",context,resp.getWriter());
//        this.springTemplateEngine.process("asdf.html",context,resp.getWriter());
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
////        super.doPost(req, resp);
////        String attributeValue = req.getParameter("id");
//        String attributeValue = req.getParameter("courseId");
//        req.getSession().setAttribute("courseId", attributeValue);
////        resp.sendRedirect("/listStudents");
//        resp.sendRedirect("/addStudent");
//    }
//}
