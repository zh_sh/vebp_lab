package mk.ukim.finki.wp.lab.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long courseId;
    String name;

    @Column(length = 4095)
    String description;

    @ManyToMany(fetch = FetchType.EAGER)
    List<Student> students;

    @ManyToMany
    List<Teacher> teachers;

    LocalDate dateEnrolled;

    @Enumerated(EnumType.STRING)
    Type type;

    public Course() {
    }

    public Course(Long courseId, String name, String description) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();
        this.dateEnrolled = null;
    }

    public Course(Long courseId, String name, String description, List<Student> students) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = students;
        this.teachers = new ArrayList<>();
        this.dateEnrolled = null;
    }

    public Course(Long courseId, String name, String description, List<Student> students, List<Teacher> teachers) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = students;
        this.teachers = teachers;
        this.dateEnrolled = null;
    }

    public Course(Long courseId, String name, String description, List<Student> students, Teacher teacher) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = students;

        this.teachers = new ArrayList<>();
        this.teachers.add(teacher);
        this.dateEnrolled = null;
    }

    public Course(String name, String description) {
        this.courseId = (long) (Math.random() * 1000);
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();
        this.teachers = new ArrayList<>();
        this.dateEnrolled = null;
    }

    public Course(Long courseId, String name, String description, Teacher teacher) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();

        this.teachers = new ArrayList<>();
        this.teachers.add(teacher);
        this.dateEnrolled = null;
    }

    public Course(Long courseId, String name, String description, Teacher teacher, LocalDate dateEnrolled) {
        this.courseId = courseId;
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();

        this.teachers = new ArrayList<>();
        this.teachers.add(teacher);
        this.dateEnrolled = dateEnrolled;
    }

    public Course(String name, String description, Teacher teacher, LocalDate dateEnrolled) {
//        this.courseId = (long) (Math.random() * 1000);
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();

        this.teachers = new ArrayList<>();
        this.teachers.add(teacher);
        this.dateEnrolled = dateEnrolled;
    }
}
