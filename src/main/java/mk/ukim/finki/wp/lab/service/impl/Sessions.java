package mk.ukim.finki.wp.lab.service.impl;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class Sessions implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
//        HttpSessionListener.super.sessionCreated(se);

        System.out.println("Session created (id:" + se.getSession().getId() + ")");
        synchronized (this){
            Integer activeUsersCount = (Integer) se.getSession().getServletContext().getAttribute("activeUsers");
//            System.out.println(activeUsersCount);
            //Long and Integer can be null, int can't be
            if(activeUsersCount != null){
//                System.out.println("not null");
                se.getSession().getServletContext().setAttribute("activeUsers", ++activeUsersCount);
            }else{
//                System.out.println("was null");
                se.getSession().getServletContext().setAttribute("activeUsers", 1);
            }
//            System.out.println(se.getSession().getServletContext().getAttribute("activeUsers"));
        }

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

        System.out.println("Session ended (id:" + se.getSession().getId() + ")");

        synchronized (this){
            Integer activeUsersCount = (Integer) se.getSession().getServletContext().getAttribute("activeUsers");
            //Long can be null, int can't be
            if(activeUsersCount != null){
                if(activeUsersCount > 0){
                    se.getSession().getServletContext().setAttribute("activeUsers", --activeUsersCount);
                }
            }
        }
    }
}
