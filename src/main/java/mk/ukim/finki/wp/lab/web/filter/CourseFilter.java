//package mk.ukim.finki.wp.lab.web.filter;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@WebFilter
//public class CourseFilter implements Filter {
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        Filter.super.init(filterConfig);
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//        String path = request.getServletPath();
//
//    if(!path.equals("/listCourses") && request.getSession().getAttribute("courseId") == null){
//            response.sendRedirect("/listCourses");
//        }
//        else if(path.equals("/StudentEnrollmentSummary") && request.getMethod().equals("POST") && request.getParameter("selectedStudent") == null){
//                response.sendRedirect("/addStudent");
//        }else{
//            filterChain.doFilter(servletRequest,servletResponse);
//        }
//
//    }
//
//    @Override
//    public void destroy() {
//        Filter.super.destroy();
//    }
//}
