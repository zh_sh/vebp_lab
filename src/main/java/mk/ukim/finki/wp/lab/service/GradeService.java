package mk.ukim.finki.wp.lab.service;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Grade;

import java.time.LocalDateTime;
import java.util.List;

public interface GradeService {
    List<Grade> listAll();
    List<Grade> listGradesByCourseId(Long courseId);
    List<Grade> listGradesByStudentUsername(String studentUsername);
    Grade addGrade(Long gradeId, Character gradeChar, String studentUsername, Long courseId, LocalDateTime timestamp);
    Grade getGradeByCourseIdAndStudentUsername(Long courseId, String studentUsername);
    List<Grade> findByTimestampBetween(LocalDateTime from, LocalDateTime to, Course c);
}
