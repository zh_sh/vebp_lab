package mk.ukim.finki.wp.lab.repository.jpa;

import mk.ukim.finki.wp.lab.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {
    Student findByUsername(String username);
//    Student findByUsernameLike(String username);
    List<Student> findAllByNameLikeOrSurnameLike(String name, String surname);
    Optional<Student> findByUsernameAndPassword(String username, String password);
}
