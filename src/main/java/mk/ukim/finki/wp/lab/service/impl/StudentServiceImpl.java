package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Role;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exception.InvalidUserCredentialsException;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryStudentRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

//    private final InMemoryStudentRepository studentRepository;
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> listAll() {
//        return studentRepository.findAllStudents();
        return studentRepository.findAll();
    }

    @Override
    public List<Student> searchByNameOrSurname(String text) {
//        return studentRepository.findAllByNameOrSurname(text);
        return studentRepository.findAllByNameLikeOrSurnameLike(text, text);
    }

    @Override
    public Student searchUsername(String username){
        return studentRepository.findByUsername(username);
    }

    @Override
    public Student save(String username, String password, String name, String surname) {
        Student s = new Student(username, password, name, surname);
        return studentRepository.save(s);
//        return s;
    }

    @Override
    public Student save(String username, String password, String name, String surname, Role role) {
        Student s = new Student(username, password, name, surname, role);
        return studentRepository.save(s);
//        return s;
    }

    @Override
    public Student login(String username, String password) {
        Optional<Student> tempStudent = studentRepository.findByUsernameAndPassword(username, password);
        System.out.println(tempStudent);
        if(!tempStudent.isPresent()){
            throw new InvalidUserCredentialsException();
        }
        else {
            return tempStudent.get();
        }
    }
}
