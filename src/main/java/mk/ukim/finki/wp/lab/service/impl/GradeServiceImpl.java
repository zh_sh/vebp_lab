package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepository;
import mk.ukim.finki.wp.lab.repository.jpa.GradeRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.GradeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GradeServiceImpl implements GradeService {

    private final GradeRepository gradeRepository;
    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    public GradeServiceImpl(GradeRepository gradeRepository, CourseRepository courseRepository, StudentRepository studentRepository) {
        this.gradeRepository = gradeRepository;
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Grade> listAll(){
        return gradeRepository.findAll();
    }

    @Override
    public List<Grade> listGradesByCourseId(Long courseId){
        Course tempCourse = courseRepository.getById(courseId);
        return gradeRepository.findAllByCourse(tempCourse);
    }

    @Override
    public List<Grade> listGradesByStudentUsername(String studentUsername){
        Student tempStudent = studentRepository.findByUsername(studentUsername);
        return gradeRepository.findAllByStudent(tempStudent);
    }

    @Override
    @Transactional
    public Grade addGrade(Long gradeId, Character gradeChar, String studentUsername, Long courseId, LocalDateTime timestamp){
        Student tempStudent = studentRepository.getById(studentUsername);
        Course tempCourse = courseRepository.getById(courseId);
        Grade tempGrade = new Grade(gradeId, gradeChar, tempStudent, tempCourse, timestamp);
        gradeRepository.save(tempGrade);
        return tempGrade;
    }

    @Override
    public Grade getGradeByCourseIdAndStudentUsername(Long courseId, String studentUsername) {
        Optional<Grade> grade = gradeRepository.findByCourseAndStudent(courseRepository.getById(courseId), studentRepository.findByUsername(studentUsername));
//        if(grade.isPresent()){
//            return grade.get();
//        }else{
//            return null;
//        }
        return grade.orElse(null);
    }

    @Override
    public List<Grade> findByTimestampBetween(LocalDateTime from, LocalDateTime to, Course course){
        return gradeRepository.findByTimestampBetween(from, to).stream().filter(r -> r.getCourse().equals(course)).collect(Collectors.toList());
    }
}
