package mk.ukim.finki.wp.lab.selenium;

import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.model.Type;
import mk.ukim.finki.wp.lab.repository.jpa.TeacherRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SeleniumScenarioTest {
    @Autowired
    private TeacherRepository teacherRepository;

    private HtmlUnitDriver driver;
    private boolean dataInitialized = false;
    private String desc = "desc";
    private LocalDate date = LocalDate.now();

    @BeforeEach
    public void setup(){
        driver = new HtmlUnitDriver(true);
        initData();
    }

    @AfterEach
    public void destroy(){
        if(driver != null){
            driver.close();
        }
    }

    private void initData(){
        if (!dataInitialized) {
            teacherRepository.save(new Teacher("testTeacher", "a", date));
            dataInitialized = true;
        }
    }

    @Test
    public void testScenario(){
        CoursesPage coursesPage = CoursesPage.openCourses(driver);
//        coursesPage = AddCoursePage.add(driver, "c0", desc, "1", date.toString(), Type.WINTER.toString());
        coursesPage.AssertElements(0, 0, 0, 0);
        LoginPage loginPage = LoginPage.openLogin(driver);
        coursesPage = LoginPage.login(driver, loginPage, "admin", "admin");
        coursesPage.AssertElements(0, 0, 0, 1);
//        coursesPage.AssertElements(1, 1, 1, 1);
        coursesPage = AddCoursePage.add(driver, "c1", desc, "1", date.toString(),
                Type.WINTER.toString());
        coursesPage.AssertElements(1, 1, 1, 1);
        coursesPage = AddCoursePage.add(driver, "c2", desc, "1", date.toString(),
                Type.WINTER.toString());
        coursesPage.AssertElements(2, 2, 2, 1);
    }
}