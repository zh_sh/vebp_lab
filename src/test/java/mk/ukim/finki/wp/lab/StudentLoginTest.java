package mk.ukim.finki.wp.lab;

import mk.ukim.finki.wp.lab.model.Role;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exception.InvalidUserCredentialsException;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.StudentService;
import mk.ukim.finki.wp.lab.service.impl.StudentServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StudentLoginTest {
    @Mock
    private StudentRepository studentRepository;

    private StudentService studentService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        Student student = new Student("username", "password", "name", "surname", Role.ROLE_USER);
        Mockito.when(studentRepository.findByUsernameAndPassword(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(java.util.Optional.of(student));
        studentService = Mockito.spy(new StudentServiceImpl(studentRepository));
    }

    @Test
    public void testSuccessLogin(){
//        Student student = studentService.login("username", "password").orElse(null);
        Student student = studentService.login("username", "password");
        Mockito.verify(studentService).login("username", "password");
        Assert.assertNotNull("Student is null", student);
        Assert.assertEquals("Password does not mach", "password", student.getPassword());
        Assert.assertEquals("Username does not mach", "username", student.getUsername());
    }

    @Test
    public void testInvalidCredentials(){
//        Assert.assertThrows("InvalidUserCredentialsException expected",
//                InvalidUserCredentialsException.class, () -> this.studentService.login(null, "password"));
//        Mockito.verify(studentService).login(null, "password");
        Assert.assertThrows("InvalidUserCredentialsException expected",
                InvalidUserCredentialsException.class, () -> this.studentService.login(null, null));
        Mockito.verify(studentService).login(null, null);
    }
}